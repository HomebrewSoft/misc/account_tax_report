# -*- coding: utf-8 -*-
{
    'name': 'Account Tax Report',
    'version': '12.0.0.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'account',
    ],
    'data': [
        # views
        'views/account_invoice_tax.xml',
        'views/account_invoice.xml',
    ],
}
