# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice.tax'

    type_related = fields.Selection(
        related='invoice_id.type',
    )
    type = fields.Selection(
        selection=[
            ('out_invoice', 'Customer Invoice'),
            ('in_invoice', 'Vendor Bill'),
            ('out_refund', 'Customer Credit Note'),
            ('in_refund', 'Vendor Credit Note'),
        ],
        compute='_get_type',
        store=True,
    )
    purchase_type_related = fields.Selection(
        related='invoice_id.purchase_type',
    )
    purchase_type = fields.Selection(
        selection=[
            ('purchase', _('Purchase')),
            ('asset', _('Asset')),
            ('service', _('Service')),
        ],
        compute='_get_purchase_type',
        store=True,
    )
    type_tax_use_related = fields.Selection(
        related='tax_id.type_tax_use',
    )
    type_tax_use = fields.Selection(
        selection=[
            ('sale', 'Sales'),
            ('purchase', 'Purchases'),
            ('none', 'None'),
            ('adjustment', 'Adjustment'),
        ],
        compute='_get_type_tax_use',
        store=True,
    )
    amount_total_stored = fields.Monetary(
        string='Amount total',
        compute='_get_amount_total_stored',
        store=True,
    )
    date_invoice = fields.Date(
        related='invoice_id.date_invoice',
    )
    state = fields.Selection(
        related='invoice_id.state',
    )

    @api.depends('type_related')
    def _get_type(self):
        for record in self:
            record.type = record.type_related

    @api.depends('purchase_type_related')
    def _get_purchase_type(self):
        for record in self:
            record.purchase_type = record.purchase_type_related

    @api.depends('type_tax_use_related')
    def _get_type_tax_use(self):
        for record in self:
            record.type_tax_use = record.type_tax_use_related

    @api.depends('amount_total')
    def _get_amount_total_stored(self):
        for record in self:
            record.amount_total_stored = record.amount_total
