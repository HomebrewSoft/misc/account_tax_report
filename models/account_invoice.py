# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    purchase_type = fields.Selection(
        selection=[
            ('purchase', _('Purchase')),
            ('asset', _('Asset')),
            ('service', _('Service')),
        ]
    )
